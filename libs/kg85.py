"""Some functions to understand blast pulse scaling if the Kinney and Graham 1985
empirical blast model. Currently this is mostly centered around Table XI, and the
corresponding interpolation functions.
"""
import pandas as pd
from pandas import DataFrame
import numpy as np
from numpy import ndarray
from numpy.typing import ArrayLike
from scipy.optimize import brentq
import asdf
import pathlib
import warnings

ot = 1 / 3
os = 1 / 6
TNT_EQUIV = 4.184e6
TNTOT = TNT_EQUIV ** ot

datapath = pathlib.Path(__file__).absolute().parent


def transmit_fd(rho0: ArrayLike = 1.225,
                p0: ArrayLike = 1.01325e5, temp0: ArrayLike = 288.15,
                **kwargs):
    """Computes the spacial transmission factor For energy scaling.

    Keyword arguments
    -----------------
    rho : ArrayLike
        Density in kg per cubic meter.
    pa : ArrayLike
        Atmospheric pressure in Pa. Needs `temp` as additional argument.
    temp : ArrayLike
        Atmospheric temperature in K. Needs `pa` as additional argument.
    """
    keys = kwargs.keys()
    errmsg = f"Valid arguments combinations are either 'rho' or" \
             f" ('pa', 'temp'). {keys} were given."
    if 'rho' in keys:
        if 'pa' in keys or 'temp' in keys:
            raise KeyError(errmsg)
        ret = (kwargs['rho'] / rho0) ** ot
    elif 'pa' in keys or 'temp' in keys:
        if 'temp' not in keys or 'pa' not in keys:
            raise KeyError(errmsg)
        ret = (kwargs['pa'] * temp0) / (p0 * kwargs['temp']) ** ot
    else:
        raise KeyError(errmsg)
    return ret


def transmit_ft(rho0: ArrayLike = 1.225, c0: ArrayLike = 340.294,
                p0: ArrayLike = 1.01325e5, temp0: ArrayLike = 288.15,
                **kwargs) -> ArrayLike:
    """Computes the time transmission factor for energy scaling.

    Keyword arguments
    -----------------
    rho : ArrayLike
        Density in kg per cubic meter. Needs `c` as additional argument.
    c : ArrayLike
        Speed of sound in m/s. Needs `rho` as additional argument.
    pa : ArrayLike
        Atmospheric pressure in Pa. Needs `temp` as additional argument.
    temp : ArrayLike
        Atmospheric temperature in K. Needs `pa` as additional argument.
    """
    keys = kwargs.keys()
    errmsg = f"Valid argument combinations are either ('rho', 'c')," \
             f" or ('pa', 'temp'). {keys} were given."
    if 'rho' in keys or 'c' in keys:
        if 'pa' in keys or 'temp' in keys:
            raise KeyError(errmsg)
        ret = (kwargs['rho'] / rho0) ** ot * kwargs['c'] / c0
    elif 'pa' in keys or 'temp' in keys:
        if 'temp' not in keys or 'pa' not in keys:
            raise KeyError(errmsg)
        ret = (kwargs['pa'] / p0) ** ot * (kwargs['temp'] / temp0) ** os
    else:
        raise KeyError(errmsg)
    return ret


class TabXi:
    """Table XI of KG85.

    Class Attributes
    ----------------
    rawdata : DataFrame
        The complete raw data of Table XI.
    rawdata_units : dict
        Units of the raw data.
    """

    # Reference properties of atmosphere
    temp0 = 288.15  # 15ºC
    p0 = 1.01325e5  # Atmospheric static pressure / Pa
    rho0 = 1.225    # Atmospheric density / kg * m**-3
    c0 = 340.294    # speed of sound / m * s**-1
    tbpars = np.array([9.80e-1, 5.40e-1, 2.00e-2, 7.40e-1, 6.90e+0])
    # pbar  1 / rbar rolloff constant for large rbar
    tb_large_rb_const = tbpars[0] * tbpars[2] ** 3 * tbpars[3] ** 6 \
        * tbpars[4] / tbpars[1] ** 10
    # Parameters for scaled pressure interpolator function
    pbpars = np.array([8.08e+2, 4.50e+0, 4.80e-2, 3.20e-1, 1.35e+0])
    # Parameters for scaled impulse interpolator function
    impbp = {
        'orig': np.array([6.70e-2, 2.30e-1, 1.00e+0, 1.55e+0]),
        'corr': np.array([5.75e-2, 2.30e-1, 1.00e+0, 1.55e+0])
    }
    _units_modes = ('SI', 'KG')
    _imp_modes = ('orig', 'corr')
    _data_columns = ['Z', 'Mx', 'pbar', 'ta_bar',
                     'sigma', 'td_bar', 'imp_bar', 'alpha']
    _data_units = {
        'SI': {'Z': 'm/J^(1/3)', 'Mx': '-', 'pbar': '-', 'ta_bar': 's/J^(1/3)',
               'sigma': 'm/s', 'td_bar': 's/J^(1/3)', 'imp_bar': 's/J^(1/3)'},
        'KG': {'Z': 'm/kg^(1/3)', 'Mx': '-', 'pbar': '-', 'ta_bar': 's/kg^(1/3)',
               'sigma': 'm/s', 'td_bar': 's/kg^(1/3)', 'imp_bar': 's/kg^(1/3)'}
    }

    def __init__(self, mode: str = 'SI'):
        """"""
        self._mode = ''
        self._impmode = ''
        self.rawdata: DataFrame = DataFrame()
        self.rawdata_units: dict = {}
        self.data: DataFrame = DataFrame()
        self.data_units: dict = {}
        self.data_available = False
        self.dpath = datapath / "kg85.asdf"
        try:
            self.load_data(self.dpath)
        except FileNotFoundError:
            warnings.warn(
                f"TabXi.__init__: {self.dpath} does not exist. "
                f"Interpolation functions will be available, bot not the "
                f"data tables."
            )
            self.data_available = False
        except:
            print(self.dpath)
            raise
        tb_large_rb_const: float = np.nan
        self.mode = mode
        self.impmode = 'corr'

    def load_data(self, dpath):
        dpath = pathlib.Path(dpath)
        if dpath.exists():
            self.dpath = dpath
        else:
            self.data_available = False
            raise FileNotFoundError(f'{dpath} does not exist.')
        af = asdf.open(dpath, mode="r")
        self.rawdata = pd.DataFrame(columns=af['tablexi']['columns'],
                                    dtype='float')
        for col in self.rawdata.columns:
            self.rawdata.loc[:, col] = af['tablexi']['data'][col][:]
        # The `data` attribute is there to provide the information of `rawdata`
        # converted to the units of the specific instance. Most important
        # columns to convert are
        # - `Z`: scaled distance (raw: m/kg^(1/3) )
        # - `ta`, `td`: arrival time and pulse duration (raw: ms)
        # - `imp`: impulse part area should be scaled with ambient pressure and
        #   energy for the time part (raw: bar ms)
        self.data: DataFrame = DataFrame()
        self.rawdata_units = {
            col: unt
            for col, unt in zip(af['tablexi']['columns'], af['tablexi']['units'])
        }
        self.data_units = {}
        af.close()
        del af
        self.data_available = True

    @property
    def impmode(self) -> str:
        return self._impmode

    @impmode.setter
    def impmode(self, val: str):
        assert val in self._imp_modes, \
            f"impmode must be one of ('orig', 'corr'), but {val} was given."
        self._impmode = val

    @property
    def impbpars(self) -> ndarray:
        return self.impbp[self.impmode]

    @impbpars.setter
    def impbpars(self, val: ndarray):
        if 'custom' not in self._imp_modes:
            self._imp_modes += ('custom',)
        self.impbp['custom'] = val

    @property
    def impb_oorb_rolloff_const(self) -> float:
        """impbar  1 / rbar rolloff constant for large rbar"""
        pars = self.impbpars
        return pars[0] * pars[2] ** 2 * pars[3] / pars[1] ** 2

    @property
    def pb_oorb_rolloff_const(self) -> float:
        """pbar 1 / rbar rolloff constant for large rbar"""
        pars = self.pbpars
        return pars[0] * pars[2] * pars[3] * pars[4] / pars[1] ** 2

    @property
    def mode(self) -> str:
        return self._mode

    @mode.setter
    def mode(self, val: str):
        assert val in self._units_modes, \
            f"mode must be one of {self._units_modes}, but {val} was given."
        self._mode = val
        print(f"Setting {val} units:")
        self.data = self.rawdata.sort_values(
            'Z', ascending=True, inplace=False, axis=0)
        if val == 'SI':
            if self.data_available:
                self.data.loc[:, 'Z'] /= TNTOT
                self.data.loc[:, ['ta', 'td']] /= 1e3 * TNTOT
                # Raw data list the un-scaled impulse per area
                # Need to convert from bar ms to Pa s for SI units, and then
                # divide by ambient pressure `p0` (* 1e5 / (1e3 * p0)) for blast
                # scaling
                self.data.loc[:, 'imp'] *= 1e2 / (TabXi.p0 * TNTOT)
            self.pbpars = TabXi.pbpars.copy()
            self.pbpars[1:] /= TNTOT  # First value in `pbpars` is unitless.
            self.tbpars = TabXi.tbpars / TNTOT
            p = self.tbpars
            self.tb_large_rb_const = \
                p[0] * p[2] ** 3 * p[3] ** 6 * p[4] * p[1] ** -10
            del p
            self.impbp = {}
            for k in TabXi.impbp.keys():
                pars = TabXi.impbp[k] / TNTOT
                pars[0] *= 1e2 / self.p0
                self.impbp[k] = pars
        elif val == 'KG':
            if self.data_available:
                self.data.loc[:, ['ta', 'td']] /= 1e3
                self.data.loc[:, 'imp'] *= 1e2 / TabXi.p0
            self.impbp = {}
            for k in TabXi.impbp.keys():
                pars = TabXi.impbp[k].copy()
                pars[0] *= 1e2 / self.p0
                self.impbp[k] = pars
        else:
            raise ValueError('Something is very wrong.')
        if self.data_available:
            self.data.columns = self._data_columns
            self.data_units = self._data_units[val]
        print("  " + "\n  ".join(self.units))

    @property
    def units(self) -> list:
        maxlen = max([len(el) for el in self.data_units.keys()])
        ret = []
        for par, unt in self.data_units.items():
            ln = len(par)
            ret.append(f"{par}: " + (maxlen - ln) * " " + unt)
        return ret

    def transmit_fd(self, **kwargs):
        """This calls `kg85.transmit_fd()` with the `TabXI` specific reference
        values. Keyword arguments behave the same as with `kg85.transmit_fd()`.

        Keyword arguments
        -----------------
        rho : ArrayLike
            Density in kg per cubic meter. Needs `c` as additional argument.
        pa : ArrayLike
            Atmospheric pressure in Pa. Needs `temp` as additional argument.
        temp : ArrayLike
            Atmospheric temperature in K. Needs `pa` as additional argument.
        """
        return transmit_fd(rho0=self.rho0, p0=self.p0, temp0=self.temp0,
                           **kwargs)

    def transmit_ft(self, **kwargs):
        """This calls `kg85.transmit_ft()` with the `TabXI` specific reference
        values. Keyword arguments behave the same as with `kg85.transmit_ft()`.

        Keyword arguments
        -----------------
        rho : ArrayLike
            Density in kg per cubic meter. Needs `c` as additional argument.
        c : ArrayLike
            Speed of sound in m/s. Needs `rho` as additional argument.
        pa : ArrayLike
            Atmospheric pressure in Pa. Needs `temp` as additional argument.
        temp : ArrayLike
            Atmospheric temperature in K. Needs `pa` as additional argument.
        """
        return transmit_ft(rho0=self.rho0, c0=self.c0,
                           p0=self.p0, temp0=self.temp0, **kwargs)

    def tbar(self, rbar: ArrayLike) -> ArrayLike:
        """Scaled blast duration from scaled distance.
         (Implements equation 6-10 of KG85)
        """
        c1, c2, c3, c4, c5 = self.tbpars
        return c1 * (1. + (rbar / c2) ** 10) / (
            (1. + (rbar / c3) ** 3)
            * (1. + (rbar / c4) ** 6)
            * (1. + (rbar / c5) ** 2) ** 0.5
        )

    def rbar(self, pbar: ArrayLike) -> ArrayLike:
        """Returns the scaled distance in units set by the `unit` property.
        This is the inverse of pbar(rbar).

        Parameters
        ----------
        pbar : ArrayLike
            Peak pressure(s), normalized by atmospheric pressure (has no units).
        """
        mn, mx = 0., 500
        # noinspection PyTypeChecker
        return brentq(
            lambda x: pbar - self.pbar(x),
            mn, mx, args=(pbar,),
            xtol=1e-3, rtol=1e-6, full_output=False
        )

    def pbar(self, rbar: ArrayLike) -> ArrayLike:
        """Computes peak pressure raatio from a given scaled distance input.

        Parameters
        ----------
        rbar : ArrayLike
            Scaled distance (in units that coincide with `unit`).
        """
        c1, c2, c3, c4, c5 = self.pbpars
        return c1 * (1. + (rbar / c2) ** 2) / (
            np.sqrt(1. + (rbar / c3) ** 2)
            * np.sqrt(1. + (rbar / c4) ** 2)
            * np.sqrt(1. + (rbar / c5) ** 2)
        )

    def imp1(self, rbar: ArrayLike) -> ArrayLike:
        """Computes scaled impulse from scaled distance input.

        Parameters
        ----------
        rbar : ArrayLike
            Scaled distance

        Returns
        -------
        Scaled impulse ( in s/kg^(1/3) or s/J^(1/3) ).
        """
        i0, c0, c1, c2 = self.impbpars
        return i0 * np.sqrt(1. + (rbar / c0) ** 4) \
            / ((rbar / c1) ** 2 * (1. + (rbar / c2) ** 3) ** ot)
