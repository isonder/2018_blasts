# -*- coding: utf-8 -*-
"""For work with crater related surfaces in map view.

author : ingo
"""
import numpy as np
from numpy import ndarray
from numpy.typing import ArrayLike
from numpy.ma import MaskedArray
from typing import Union, Callable
from scipy.ndimage import interpolation as intp

SQRT3 = np.sqrt(3.)
PI = np.pi


def af_line(beta: ArrayLike) -> ArrayLike:
    """Footprint area of a linear blast line consisting of three lateral
    centers.

    Parameters
    ----------
    beta : ndarray or float
        Ratio of (lateral) explosive charge distance to crater radius.
    """
    retconv = False
    if np.isscalar(beta):
        beta = np.array([beta])
        retconv = True
    ret = np.empty_like(beta, dtype=np.float)
    idx = beta < 2.
    if np.any(idx):
        bta = beta[idx]
        ret[idx] = (3 * PI - 4 * np.arccos(.5 * bta)
                    + bta * np.sqrt(4. - bta ** 2))
    idx = beta >= 2.
    if np.any(idx):
        ret[idx] = 3. * PI
    return ret[0] if retconv else ret


def af_triag(beta: ArrayLike) -> ArrayLike:
    """Footprint area of a triangular blast arrangement consisting of three
     lateral centers.

    Parameters
    ----------
    beta : ndarray or float
        Ratio of (lateral) explosion distance to crater radius.
    """
    def _c_(bta):
        return 0.25 * bta * (1. + np.sqrt(12 / (bta ** 2) - 3.))

    reduce = False
    if np.isscalar(beta):
        beta = np.asarray([beta])
        reduce = True
    ret = np.empty_like(beta)
    idx = beta < 1
    if np.any(idx):
        c = _c_(beta[idx])
        ret[idx] = SQRT3 * c ** 2 + 3 * (np.arcsin(c) - c * np.sqrt(1 - c ** 2))
    idx = np.logical_and(beta >= 1, beta <= SQRT3)
    if np.any(idx):
        c = _c_(beta[idx])
        ret[idx] = SQRT3 * c ** 2 + 3 * (np.pi - np.arcsin(c)
                                         + c * np.sqrt(1 - c ** 2))
    idx = np.logical_and(beta >= SQRT3, beta <= 2)
    if np.any(idx):
        btah = .5 * beta[idx]
        ret[idx] = 3 * np.pi \
            - 6 * (np.arccos(btah) - btah * np.sqrt(1 - btah ** 2))
    idx = beta >= 2
    if np.any(idx):
        ret[idx] = 3 * np.pi
    return ret[0] if reduce else ret


def footprint_area(radius: ArrayLike, b: float,
                   af: Callable = af_line) -> ArrayLike:
    """Calculate absolute footprint area from 'relative' footprint `af`

    Parameters
    ----------
    radius : array_like
        Radius constraint.
    b : float
        Lateral explosion distance.
    af : Callable, optional
        Relative footprint function.

    Returns
    -------
    Returns `af(b / radius) * radius ** 2`.
    """
    return af(b / radius) * radius ** 2


def rotate_raster(rdata: ndarray, angle: float,
                  pivot: Union[list, tuple]) -> MaskedArray:
    """Rotate a raster array by an angle around a pivot point.

    Parameters
    ----------
    rdata : ndarray
      Raster to rotate.
    angle : float
        Rotation angle.
    pivot : tuple or list
        Two (y, x)-numbers specifying the pivot point.

    Returns
    -------
    Rotated raster as masked array.
    """
    ly, lx = rdata.shape
    py, px = pivot
    pady = [0, 2 * py - ly] if 2 * py >= ly else 2 * [ly - 2 * py]
    padx = [0, 2 * px - lx] if 2 * px >= lx else 2 * [lx - 2 * px]
    fill_val = rdata.data.min()
    ret = np.pad(rdata, [pady, padx], mode='constant',
                 constant_values=fill_val)
    ret = intp.rotate(ret, angle=angle, reshape=False,
                      mode='constant', cval=fill_val)
    ret = MaskedArray(
        data=ret,
        mask=np.logical_or(ret < rdata.min(), ret > rdata.max()),
        fill_value=fill_val
    )
    if rdata.dtype.kind in ('u', 'i'):
        info = np.iinfo(rdata.dtype)
    else:
        info = np.finfo(rdata.dtype)
    ret[ret.mask] = info.max
    cpy, cpx = ret.shape
    cpy //= 2
    cpx //= 2
    starty = cpy - py
    endy = starty + ly
    startx = cpx - px
    endx = startx + lx
    ret = ret[starty:endy, startx:endx]
    return ret
