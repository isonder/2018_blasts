from setuptools import setup
import sys

ver = sys.version[:3]
pth = f"/usr/lib/python{ver}/site-packages"
if pth not in sys.version:
    sys.path.append(pth)

setup(
    name="twenty_eighteen_blasts",
    version="0.1",
    description="Helper funcs for blast analysis.",
    url='None',
    author='ingo',
    author_email='ingomark@buffalo.edu',
    license='GPLv2',
    py_modules=['mapview', 'kg85', 'micutils'],
    install_requires=['numpy', 'scipy', 'asdf', 'pandas', 'matplotlib']
)
