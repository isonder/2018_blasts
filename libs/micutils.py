# -*- coding: utf-8 -*-
"""Functionality for analysis of microphone records of blast waves.

author : ingo
"""
import numpy as np
from numpy import ndarray, nan, isnan
from numpy.typing import ArrayLike
from typing import Tuple, Union, Iterable
import scipy.signal as sig
from scipy.optimize import curve_fit
from matplotlib.figure import Figure, Axes
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

debug = False


class TimeNotFoundError(Exception):
    pass


def filter_sig(signal, fs: float, fmax: float = 1e4, order: int = 4):
    """Filters `signal` to help reduce noise and find zero crossing. Currently
    this is a high pass. Uses `sig.filtfilt()`, therefore the `order` parameter
    has to be divisible by 2.

    Parameters
    ----------
    signal : ndarray
    fs : float
        Sampling rate (in Hz or similar)
    fmax : float, optional
        The high pass frequency.
    order : int, optional
        Order of the filter. Must be 2-divisible.

    Returns
    -------
    ndarray
        The filtered signal.
    """
    assert order % 2 == 0, f"order has to be divisible by 2, but I got a " \
                           f"value {order}."
    order //= 2
    b, aa = sig.bessel(order, fmax / fs, btype='low', analog=False)
    signal = sig.filtfilt(b, aa, signal)
    return signal


def tstart(t: ndarray, p: ndarray, windows: ndarray,
           a: Union[ndarray, float], fs: float, pfilt: ndarray = None):
    """Find arrival times within a rough window based on a threshold `a` given
    as multiples of the background noise level's standard deviation. An
    `IndexError` is raised, if the threshold specified by `a` was not found.
    
    Parameters
    ----------
    t : ndarray
        Time axis.
    p : ndarray
        Pressure signal.
    windows : ndarray
    a : ndarray or float
        Threshold parameter in multiples of the (local) signal's standard
        deviation.
    fs : float
        Sampling rate.
    pfilt : ndarray (optional)

    Returns
    -------
    ndarray
        Detected arrival time
    """
    assert t.shape == p.shape, "t and p must have same shape."
    ret = np.empty(len(windows), dtype=float)
    try:
        iter(a)
    except TypeError:
        a = np.asarray(len(windows) * [a], dtype=float)
    for i, ((start, end), aa) in enumerate(zip(windows, a)):
        istart = int(fs * start)
        imean = int(fs * 0.1)  # interval for averaging
        # standard deviation of the 'unfiltered' signal local to but before
        # arrival
        std = p[:imean].std()
        if pfilt is None:
            sel = filter_sig(p[int(fs * start):int(fs * end)], fs=fs)
        else:
            sel = pfilt[i].copy()
        sel -= sel[:int(fs * .01)].mean()
        ts = t[np.where(sel >= a[i] * std)[0][0] + istart]
        ret[i] = ts
    return ret


def delta_t(da: Tuple[ndarray, ndarray], db: Tuple[ndarray, ndarray],
            windows: ndarray, a: ArrayLike, fs: float):
    return tstart(db[0], db[1], windows, a, fs) \
            - tstart(da[0], da[1], windows, a, fs)


def ctimes(t: ndarray, p: ndarray, tarr: ArrayLike,
           windows: ndarray, a: ArrayLike, fs: float,
           pfilt: ndarray = None) -> Tuple[ndarray, ndarray, ndarray]:
    """Based on a 'rough' arrival estimate, compute characteristic times for
    a pressure pulse: `ts`: a refined start time estimate, `t1`: time of first
    zero pressure after peak, `t2`: time of second zero pressure after peak.

    Parameters
    ----------
    t : ndarray
        time axis
    p : ndarray
        pressure
    tarr : ndarray or float
        arrival time estimate
    windows : ndarray
        time intervals to consider.
    a : ndarray or float
        multiples if noise standard deviation as used to estimate `tarr`.
    fs : float
        sampling frequency.
    pfilt : ndarray
        filtered pressure signal.

    Returns
    -------
    The tuple (ts, t1, t2)
    """
    assert t.shape == p.shape, "t and p must have same shape."
    try:
        iter(a)
    except TypeError:
        a = np.asarray(len(windows) * [a], dtype=float)
    ts, t1, t2 = np.empty(len(tarr)), np.empty(len(tarr)), np.empty(len(tarr))
    start = np.asarray(fs * windows[:, 0], dtype=int)
    end = np.asarray(fs * windows[:, 1], dtype=int)
    if pfilt is None:
        pfilt = []
        for i, (ist, ien) in enumerate(zip(start, end)):
            pfilt.append(filter_sig(p[ist:ien], fs=fs))
    for i, window in enumerate(windows):
        istart, iend = start[i], end[i]
        itarr = int(fs * tarr[i])
        std = p[istart:itarr].std()
        arrlev = a[i] * std
        # use unfiltered signal to get the peak pressure
        pp, idx = p[istart:iend].max(), p[istart:iend].argmax()
        p0bar = pfilt[i][(itarr - istart) // 2:int(0.8 * (itarr - istart))].mean()
        tmax = t[istart + idx]
        try:
            tone = t[np.where(pfilt[i][idx:] < 0.)[0][0] + idx + istart]
        except IndexError:
            print('t1 not found, pulse', i, '  idx:', idx,
                  '  pmax:', pp, '  pmin:', pfilt[i][idx:].min())
            raise
        idx = pfilt[i].argmin()
        try:
            ttwo = t[np.where(pfilt[i][idx:] >= 0.)[0][0] + idx + istart]
        except IndexError:
            print('pulse', i, '  idx:', idx, '  pmin:', pfilt[i][idx],
                  'pmax (after min):', pfilt[i][idx:].max())
            ttwo = windows[i, 1]
        ts[i] = tarr[i] - ((arrlev - p0bar) / (pp - arrlev)) * (tmax - tarr[i])
        t1[i], t2[i] = tone, ttwo
    return ts, t1, t2


def plot_pulseset(figax, t, p, wd, fs, figure: bool = True):
    if figure:
        fig = figax
        axs = figax.subplots(nrows=len(wd) // 2, ncols=2,
                             sharex='none', sharey='all')
    else:
        axs = figax
        fig = axs[0].figure
    start = np.asarray(wd[:, 0] * fs, dtype=np.int)
    end = np.asarray(wd[:, 1] * fs, dtype=np.int)
    for s, e, ax in zip(start, end, axs.flatten()):
        ax.plot(t[s:e], p[s:e])
    axxs = axs[:, 0] if axs.ndim > 1 else [axs[0]]
    for ax in axxs:
        ax.set_ylabel(r'$p\ /\ \mathrm{Pa}$')
    axxs = axs[:, 1] if axs.ndim > 1 else axxs
    for ax in axxs:
        ax.set_xlabel(r'$t\ /\ \mathrm{s}$')
    fig.subplots_adjust(top=0.94, bottom=0.11, left=0.075, right=0.95,
                        hspace=0.2, wspace=0.1)
    fig.set_size_inches(8, 8)


class PulseBase:
    """..."""
    def __init__(self, t: ndarray, p: ndarray):
        self.t = t
        # sampling rate
        self.fs: float = (t.shape[0] - 1) / (t[-1] - t[0])
        self.dt: float = 1 / self.fs
        self.p: ndarray = p
        self.psq_noise_level = np.mean(self.p[:int(self.fs * 0.1)] ** 2)


class Pulse:
    """Container to combine data and methods to analyze a blast pulse signal.
    
    Parameters
    ----------
    t: ndarray
        Time axis
    p: ndarray
        Pressure signal
    pp: float
        peak pressure
    window: iterable
        time interval of interest
    tm: tuple
        tuple of characteristic times (`ts`, `tmax`, `t1`, `t2`)
    a: float
        Detection threshold.
    base: PulseBase, optional
        PulseBase to use as shared base instance. Creates a new base if this is
        `None`.
    """
    def __init__(self, t: ndarray, p: ndarray, pp: float,
                 window: Union[list, tuple, ndarray], tm: tuple, a: float,
                 base: PulseBase = None):
        self.base = PulseBase(t, p) if base is None else base
        # peak pressure
        self.pp = pp
        self.window = window
        # self.tm = tm
        self.a = a
        self.ts, self.tmax, self.t1, self.t2 = tm
        self._imp = nan
        self._imp1 = nan
        self._imp2 = nan
        self._m1 = nan
        self._m2 = nan
        self._psqbar = nan
        self._impulse_integral = nan
        self._pfilt = np.array(nan)
        self._impulse_integral = nan
        self._alpha = nan
        self._ppf46 = nan

    def _get_start_end(self, ta: float, tb: float) -> Tuple[int, int]:
        return int(self.base.fs * ta), int(self.base.fs * tb)

    def _integrate(self, signal: ndarray, a: int, b: int):
        return np.trapz(y=signal[a:b], dx=1 / self.base.fs)

    @property
    def pfilt(self) -> ndarray:
        """Filtered signal (currently filtered with a 10kHz low-pass)."""
        if isnan(self._pfilt):
            start, end = self._get_start_end(*self.window)
            self._pfilt = filter_sig(self.base.p[start:end])
        return self._pfilt

    @property
    def imp1(self) -> float:
        """Impulse of the positive pulse duration."""
        if isnan(self._imp1):
            start, end = self._get_start_end(self.ts, self.t1)
            self._imp1 = np.trapz(self.base.p[start:end], dx=self.base.dt)
        return self._imp1

    @property
    def imp2(self) -> float:
        """Impulse of the negative pulse duration."""
        if isnan(self._imp2):
            start, end = self._get_start_end(self.t1, self.t2)
            self._imp2 = np.trapz(self.base.p[start:end], dx=self.base.dt)
        return self._imp2

    @property
    def imp(self) -> float:
        """Total signal impulse (integrated over interval starting at `ts`,
         and ending at end of interval).
         """
        if isnan(self._imp):
            start, end = self._get_start_end(self.ts, self.window[1])
            self._imp = np.trapz(self.base.p[start:end], dx=self.base.dt)
        return self._imp

    @property
    def m1(self) -> float:
        """First moment."""
        if isnan(self._m1):
            self._m1 = 2. * self.imp1 / ((self.t1 - self.ts) * self.pp)
        return self._m1

    @property
    def m2(self) -> float:
        """Second moment."""
        if isnan(self._m2):
            start, end = self._get_start_end(self.ts, self.window[1])
            self._m2 = 2. * (
                np.trapz(self.base.p[start:end] ** 2, dx=self.base.dt)
                - self.base.psq_noise_level * (self.window[1] - self.ts)
            ) / ((self.t1 - self.ts) * self.pp ** 2)
        return self._m2

    def impulse(self, t_end: float) -> ndarray:
        """Impulse of signal. Time integral of pressure. Integration stars at
        time `ts`, and ends at time `t_end`.
        
        Parameters
        ----------
        
        t_end : float
            End time of impulse interval.

        Returns
        -------
        ndarray
            Transient impulse values starting at time *t* = `ts`.
        """
        start, end = self._get_start_end(self.ts, t_end)
        return np.cumsum((self.base.p[start:end - 1]
                          + self.base.p[start + 1:end]) / (2 * self.base.fs))

    @property
    def impulse_integral(self) -> float:
        """...
        """
        if isnan(self._impulse_integral):
            start, end = self._get_start_end(self.ts, self.window[:, 1])
            self._impulse_integral = np.trapz(
                self.impulse(self.window[1]), start, end)
        return self._impulse_integral

    def f46(self, x, alph) -> ndarray:
        """Returns a Friedlander pulse with decay constant `alph`.
        pp * (1 - x) * exp(-alph * x)
        Peak pressure `pp` is calculated from the `Pulse`s positive impulse
        `imp1`.
        
        Parameters
        ----------
        
        x : array_like
            Independent variable
        alph : float
            Decay constant
        """
        pp = alph ** 2 * self.imp1 / (
                (self.t1 - self.ts) * (alph + (np.e ** -alph)) - 1.)
        return pp * (1. - x) * np.exp(-alph * x)

    @property
    def alpha(self) -> float:
        """Decay constant."""
        if isnan(self._alpha):
            start, end = self._get_start_end(self.ts, self.t1)
            x, p = self.base.t[start:end].copy(), self.base.p[start:end].copy()
            x = (x - self.ts) / (self.t1 - self.ts)
            res = curve_fit(self.f46, x, p, p0=[1.], bounds=(.05, 5.))
            self._alpha = res[0][0]
        return self._alpha

    @property
    def ppf46(self) -> float:
        """Peak Pressure, derived from impulse signal and assumption of a
         Friedlander 46 pulse pressure shape.
        """
        if isnan(self._ppf46):
            alph = self.alpha
            self._ppf46 = alph ** 2 * self.imp1 / (
                (self.t1 - self.ts) * (alph + np.e ** -alph - 1.)
            )
        return self._ppf46

    def plot_pulse(self, ax: Axes, model: bool = True,
                   labels: bool = True, legend: bool = False):
        start, end = self._get_start_end(*self.window)
        t, p = self.base.t[start:end], self.base.p[start:end]
        ax.scatter(t, p, s=3, color='gray', label='data')
        if labels:
            ax.set_xlabel(r'$t\ /\ \mathrm{s}$')
            ax.set_ylabel(r'$p\ /\ \mathrm{Pa}$')
        if model:
            ax.plot([t[0], self.ts, self.ts], [0., 0., self.ppf46],
                    color='#f58585')
            tt = np.linspace(self.ts, self.t1, 300)
            xp = (tt - self.ts) / (self.t1 - self.ts)
            yp = self.ppf46 * (1. - xp) * np.exp(-self.alpha * xp)
            ax.plot(tt, yp, color='#f58585', label='F46')
            tt = np.linspace(self.t1, t[-1], 300)
            xp = (tt - self.ts) / (self.t1 - self.ts)
            yp = self.ppf46 * (1. - xp) * np.exp(-self.alpha * xp)
            ax.plot(tt, yp, color='#f58585')
        if legend:
            ax.legend(loc='upper right')
        ax.set_ylim(1.1 * p.min(), 1.1 * max(self.ppf46, self.pp))


class Pulseset:
    """A container to store a pressure signal of a set of pulses generated by an
    explosion or similar.
     
    Parameters
    ----------
    t: ndarray
        time axis
    p: ndarray
        pressure signal
    pp: ndarray
        peak pressure
    window: ndarray
        An array of start and end times that bracket each pulse.
    tm: iterable
        A tuple of (ts, tmax, t1, t2) values, where ts, tmax, t1, t2 can be
        either single values or arrays.
    a: ndarray or float, optional
        Detection threshold.
    """
    adapt_limit: float = 10.

    def __init__(self, t: ndarray, p: ndarray, pp: ArrayLike,
                 window: ndarray, tm: Union[Tuple, ndarray] = None,
                 a: ArrayLike = 5.):
        self._pfilt = None
        ts, tmax, t1, t2 = [self._as_array(tt) for tt in tm]
        self.base = PulseBase(t, p)
        if isinstance(a, float):
            a = np.full_like(ts, a)
        pp = np.asarray(pp)
        self._pulses = np.array([
            Pulse(t, p, pp[i], window[i], (ts[i], tmax[i], t1[i], t2[i]), a[i],
                  base=self.base)
            for i in range(ts.shape[0])
        ])
        self.tarr = None
        self.npulse = self._pulses.shape[0]
        assert window.shape[0] == self.npulse, \
            "Shape mismatch: got window.shape=%s, but ts.shape=%s" % \
            (window.shape, ts.shape)
        self.pulseno = np.arange(self.npulse) + 1

    def __repr__(self):
        s = "Pulse Set, pmax: "
        for p in self.pp:
            s += "%.3e,  " % p
        s = s[:-3]
        return s

    def _repr_html_(self):
        s = f"<pre>{self.__repr__()}</pre><br/>"
        return s

    @staticmethod
    def _as_array(val: Union[float, Iterable]) -> ndarray:
        val = [val] if isinstance(val, float) else val
        return np.asarray(val)

    def _get_start_end(self, a: ndarray, b: ndarray) -> Tuple[ndarray, ndarray]:
        start = np.asarray(self.base.fs * a, dtype=int)
        end = np.asarray(self.base.fs * b, dtype=int)
        return start, end

    @property
    def ts(self) -> ndarray:
        """Start times."""
        return np.asarray([pl.ts for pl in self._pulses])

    @property
    def t1(self) -> ndarray:
        """First roots of pulse signals after start."""
        return np.asarray([pl.t1 for pl in self._pulses])

    @property
    def t2(self) -> ndarray:
        """Second roots of pulse signals after start."""
        return np.asarray([pl.t2 for pl in self._pulses])

    @t2.setter
    def t2(self, value: ndarray):
        for pl, ttwo in zip(self._pulses, value):
            pl.t2 = ttwo

    @property
    def pp(self) -> ndarray:
        """Peak pressures."""
        return np.asarray([pl.pp for pl in self._pulses])

    @property
    def windows(self) -> ndarray:
        """Time intervals of interest."""
        return np.asarray([pl.window for pl in self._pulses])

    @property
    def pfilt(self) -> ndarray:
        """Filtered and windowed pressure signals used in search routines.
        """
        return np.asarray([pl.pfilt for pl in self._pulses])

    @property
    def imp1(self) -> ndarray:
        """Impulse of the positive pulse interval (ts, t1)"""
        return np.asarray([pl.imp1 for pl in self._pulses])

    @property
    def imp2(self) -> ndarray:
        """Impulse of the negative pulse interval (t1, t2)"""
        return np.asarray([pl.imp2 for pl in self._pulses])

    @property
    def imp(self) -> ndarray:
        """Impulse integrated from pulse start to end of window."""
        return np.asarray([pl.imp for pl in self._pulses])

    @property
    def m1(self) -> ndarray:
        """First moments."""
        return np.asarray([pl.m1 for pl in self._pulses])

    @property
    def m2(self) -> ndarray:
        """Second moments."""
        return np.asarray([pl.m2 for pl in self._pulses])

    @property
    def impulse_integral(self) -> ndarray:
        """..."""
        return np.asarray([pl.impulse_integral for pl in self._pulses])

    @property
    def alpha(self) -> ndarray:
        return np.asarray([pl.alpha for pl in self._pulses])

    @property
    def ppf46(self) -> ndarray:
        return np.asarray([pl.ppf46 for pl in self._pulses])

    def plot_times(self, fig: Figure, filtered: bool = False):
        axs = fig.subplots(nrows=int(np.ceil(len(self.t1) / 2)), ncols=2,
                           sharex='none', sharey='all')
        start, end = self._get_start_end(self.windows[:, 0], self.windows[:, 1])
        lmt = (self.base.p[start.min():end.max()].max(),
               1.5 * self.base.p[start.min():end.max()].min())
        tm = [self.ts, self.t1, self.t2]
        inaxs = []
        plot_inset = True
        for i, (s, e, ts, t1, t2, ax) in enumerate(zip(start, end, *tm,
                                                       axs.flatten())):
            ax.plot(self.base.t[s:e], self.base.p[s:e])
            # the inset axes
            try:
                ins = int(self.base.fs * (ts - .1 * (t1 - ts)))
                ine = int(self.base.fs * (ts + 0.25 * (t1 - ts)))
                inax = inset_axes(ax, width='40%', height='33%',
                                  loc='lower right', borderpad=2)
                inax.plot(self.base.t[ins:ine], self.base.p[ins:ine])
            except ValueError:
                plot_inset = False
            if filtered:
                ax.plot(self.base.t[s:e], self.pfilt[i])
                if plot_inset:
                    inax.plot(self.base.t[ins:ine],
                              self.pfilt[i][ins - s:ine - s])
            ax.vlines(ts, *lmt, colors='g', label=r'$t_s$')
            if plot_inset:
                inax.vlines(ts, *inax.get_ylim(), colors='g')
                inaxs.append(inax)
            ax.vlines(t1, *lmt, colors='b', label=r'$t_1$')
            ax.vlines(t2, *lmt, colors='k', label=r'$t_2$')
        if self.tarr is not None:
            for tarr, ax, inax in zip(self.tarr, axs.flatten(), inaxs):
                ax.vlines(tarr, *lmt, colors='r', label=r'$t_\mathrm{arr}$')
                inax.vlines(tarr, *inax.get_ylim(), colors='r')
        for ax in axs[:, 0]:
            ax.set_ylabel(r'$p\ /\ \mathrm{Pa}$')
        for ax in axs[-1, :]:
            ax.set_xlabel(r'$t\ /\ \mathrm{s}$')
        fig.subplots_adjust(top=0.94, bottom=0.11, left=0.085, right=0.95,
                            hspace=0.2, wspace=0.1)
        fig.set_size_inches(8, 8)
        axs[0, -1].legend(loc='upper left', bbox_to_anchor=(1.02, 1.0))

    def plot_f46(self, fig: Figure):
        fig.set_size_inches(8, 8)
        fig.subplots_adjust(left=0.125, right=0.975, bottom=0.125,
                            top=0.9, wspace=0.1, hspace=0.2)
        axs = fig.subplots(nrows=int(np.ceil(len(self.t1) / 2)), ncols=2,
                           sharex='none', sharey='none')
        for i, ax in zip(range(len(self.pulseno)), axs.flatten()):
            self._pulses[i].plot_pulse(ax, labels=False, legend=False)
            ax.text(.98, .96, 'Blast %d' % self.pulseno[i],
                    ha='right', va='top', transform=ax.transAxes)
        for ax in axs[:, 0]:
            ax.set_ylabel(r'$p\ /\ \mathrm{Pa}$')
        for ax in axs[-1, :]:
            ax.set_xlabel(r'$t\ /\ \mathrm{s}$')
        points, line = None, None
        ax = axs[0, 0]
        for ch in ax.get_children():
            if ch.get_label() == 'data':
                points = ch
            if ch.get_label() == 'F46':
                line = ch
            if points is not None and line is not None:
                break
        axs[0, -1].legend(handles=(points, line), labels=('data', 'F46'),
                          loc='upper left', bbox_to_anchor=(1.02, 1.))
