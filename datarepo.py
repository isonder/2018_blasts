"""User ready data structures of the 2018 blasts. 

Data Structures
---------------
byu_pchmap : dict
    This dictionary maps the index of `pos` to channel numbers of the
    BYU microphone dataset. For many positions there are several channels,
    because BYU mounted microphone pairs at two different heights at a
    location.
byu_chpmap : dict
    The 'reverse' version of `byu_pchmap`. Takes a byu microphone
    channel number, and maps it to the correct index of the `pos` dataframe.
byu_ch : dict
    Selected microphone channel groups.
ub_data : AsdfFile
    Top level asdf file object to access the UB sensors data.
xyz : list
    Convenience symbol to access the ['x', 'y', 'z'] columns.
lxyz : list
    Convenience symbol to access the ['label', x', 'y', 'z'] columns.
xy : list
    Convenience symbol to access the ['x', 'y'] columns.
lxy : list
    Convenience symbol to access the ['label', x', 'y'] columns.

Getter Functions:
-----------------
get_byu_mics : AsdfFile or dict
    Returns the top-level asdf file object of BYU microphone data, or a
    dictionary with the microphone data.
get_byu_mics_summ : DataFrame
    Returns a summary of BYU microphone data.
get_pos : Dataframe
    Returns sensor positions in the site coordinate system. The index
    corresponds to the measured point number.
"""
from pathlib import Path
import pandas as _pd
import numpy as _np
from scipy.io import matlab as _ml
try:
    import asdf as _asdf
except ImportError:
    _asdf = None

dataloc = Path('../data')


class MatLoader:
    typ = dict
    
    def __init__(self, **kwds):
        self.dct = {}
        for k, v in kwds.items():
            try:
                self.dct[k] = self._chkpth(v)
            except ValueError:
                print(f"Matloader.__init__: skipping {k}: {v}")
            except TypeError as e:
                print(e.msg)
                print(f"Matloader.__init__: skipping {k}.")
        self.keys = self.dct.keys
        self.values = self.dct.values
        self.items = self.dct.items

    @staticmethod
    def _chkpth(pth) -> Path:
        if isinstance(pth, str):
            pth = Path(pth)
        if isinstance(pth, Path):
            if pth.exists():
                return pth
            else:
                raise ValueError(f"Matloader._chkpth: invalid path: {pth}")
        else:
            raise TypeError(f"Got a wrong type for {pth}: {type(pth)}.")
    
    def __getitem__(self, key):
        v = self.dct[key]
        return v if isinstance(v, dict) else _ml.loadmat(str(self.dct[key]))
    
    def __setitem__(self, key, val):
        self.dct[key] = self._chkpth(val)
    
    def __repr__(self):
        return self.dct.__repr__()
    
    def __str__(self):
        return self.dct.__str__()


def get_byu_mics(loc: Path = None):
    loc = dataloc / 'BYU Acoustics/Data/asdf/byu-mics.asdf' if loc is None else Path(loc)
    if loc.suffix == '.asdf':
        if loc.exists():
            ret = _asdf.open(loc, mode='rw')
            ret.find_references()
        else:
            ret = get_byu_mics(dataloc / 'BYU Acoustics/Data/Aligned')
    elif loc.is_dir():
        ret = {}
        for i in range(1, 5):
            pad, ldr = f"pad{i}", MatLoader()
            fld = loc / f"Pad {i}"
            for el in fld.glob("*.mat"):
                ch = int(el.name.rsplit("Ch")[1].rsplit(".")[0])
                ldr[ch] = el
            ret[pad] = ldr
    else:
        raise ValueError(f"Cannot find location {loc}.") 
    return ret


def get_byu_mics_summ(loc: Path = None):
    loc = dataloc/'BYU Acoustics/Data/asdf/byu-mics.asdf' if loc is None else Path(loc)
    af = _asdf.open(loc, mode='rw')
    af.find_references()
    ret = _pd.DataFrame(
        {col: af['summ'][col][:] for col in af['summ']['cols']},
        index=_pd.RangeIndex(af['summ']['pad'].shape[0])
    )
    return ret


def get_pos(loc: Path = None):
    loc = dataloc / 'UB/storage/storage.asdf' if loc is None else Path(loc)
    if loc.suffix == '.asdf':
        if loc.exists():
            ubd = _asdf.open(loc, mode='rw')
            ret = _pd.DataFrame(
                index=_pd.Index(ubd['positions']['index'], name='point'))
            for col in ubd['positions']['columns']:
                ret.loc[:, col] = ubd['positions']['values'][col][:]
        else:
            ret = get_pos(dataloc / 'UB/positions.txt')
    else:
        ret = _pd.read_csv(loc, sep='\t', index_col=0)
    return ret


_asdf_opts = dict(all_array_storage='external', all_array_compression='zlib')


def get_morph(loc: Path = None):
    loc = dataloc / 'UB/storage/crater-morphology.asdf' if loc is None else Path(loc)
    return _asdf.open(loc, mode='r')


def get_chargepos(loc: Path = None):
    loc = dataloc / 'UB/storage/crater-morphology.asdf' if loc is None else Path(loc)
    af = _asdf.open(loc, mode='r')
    ret = _pd.DataFrame()
    ret.__doc__ = af['charge_coords']['comment']['doc']
    for col in af['charge_coords']['columns']:
        ret[col] = af['charge_coords'][col][:]
    af.close()
    return ret


byu_pchmap = {
    'pad1': {
        51: [0, 1], 50: [2, 3], 49: [4, 5], 48: [12, 13],
        47: [10, 11], 46: [8, 9], 45: [6, 7], 53: [14, 15],
        57: [16, 17], 54: [22]
    },
    'pad2': {
        51: [0, 1], 50: [2, 3], 49: [4, 5], 48: [12, 13],
        47: [10, 11], 46: [8, 9], 45: [6, 7], 53: [14, 15],
        57: [16, 17], 54: [22]
    },
    'pad3': {
        51: [0, 1], 50: [2, 3], 49: [4, 5], 48: [12, 13],
        47: [10, 11], 46: [8, 9], 45: [6, 7], 53: [14, 15],
        57: [16, 17], 54: [22]
    },
    'pad4': {
        51: [0, 1], 50: [2, 3], 49: [4, 5], 48: [12, 13],
        47: [10, 11], 46: [8, 9], 45: [6, 7], 53: [14, 15],
        57: [16, 17], 54: [22]
    },
}
byu_chpmap = {}
for pad in ['pad' + str(i) for i in (1, 2, 3, 4)]:
    for pnt, chs in byu_pchmap[pad].items():
        for ch in chs:
            if ch not in byu_chpmap.keys():
                byu_chpmap[ch] = {}
            byu_chpmap[ch][pad] = pnt

# selected channel groups
byu_ch = {
    'rline': {
        'h': _np.array([7, 22, 17, 19, 21, 25, 27, 29]),
        'l': _np.array([6, 15, 14, 16, 18, 20, 24, 26, 28])
    },
    'arc': {
        'h': _np.array([1, 3, 5, 7, 9, 11, 13]),
        'l': _np.array([0, 4, 6, 8, 10, 12])
    }
}

xyz = ['x', 'y', 'z']
lxyz = ['label'] + xyz
xy = ['x', 'y']
lxy = ['label'] + xy

# line numbers (index of the dataframe returned from get_pos()) for some often
# used points.
posmap = {
    # Location of surface of charge holes for each pad.
    'charges': {1: [6, 7, 8], 2: [10, 11, 12], 3: [13, 14, 15], 4: [17, 18, 19]}
}