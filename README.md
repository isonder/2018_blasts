# `2018_blasts`

Some of the data analysis of the 2018 NSF Large Scale Experiment Workshop on Volcanic Blasts.
Several explosive charges were detonated at 0.5 seconds delay in prepared test pads made from unconsolidated rock material, arranged in different geometries. 
There is an [article in EOS](https://doi.org/10.1029/2018EO109237) and a [manuscript on EarthArXiv](https://doi.org/10.31223/X55W4F) that cover more details on what we did.
Collected sensor records, videos and notes are hosted as [dataset on Zenodo](https://doi.org/10.5281/zenodo.5842607).


## Authors and Acknowledgment

The workshop was very much a team effort, and this repo only shows part of the ongoing data analysis.

*Main workshop authors/contributors:*  
Ingo Sonder,
Alison Graettinger,
Tracianne B. Neilsen,
Robin S. Matoza,
Jacopo Taddeucci,
Julie Oppenheimer,
Einat Lev,
Kae Tsunematsu,
Greg Waite,
Greg A. Valentine


## Working With This Code

Data in the zip archives on Zenodo is meant to be placed in subfolders of a common top level directory.
Then this analysis pack can be cloned/downloaded into a folder in this top level location, e.g. called
`anl` (or `analysis`)
```bash
git clone https://gitlab.com/isonder/2018_blasts.git anl
```

The `.ipynb` notebooks contain Python code that modifies raw data and/or analyses some dataset.
Most plots are contained in the notebooks, and they can be viewed in the browser on Gitlab or [NBviewer](https://nbviewer.org).
The analysis was done using common, widely available general scientific and geo-scientific Python libraries (see below).
To run the notebooks I recommend to proceed in the usual steps and install the libraries in a separate virtual environment.
Probably the most fragile dependencies are `rasterio`, `shapely`, `fiona` and `gdal`.
The latter tends to be sometimes painful when not running on conda.
The steps below show what I think should work in order to get going.

Data in the Zenodo repository comes in different formats.
In order to save memory, and access data in a relatively consistent way the analysis converts 'raw' data to the ASDF ('[A]dvanced [S]cientific [D]ata) format (*Not to be confused with the Adaptable Seismic Data Format*).
[ASDF](https://asdf.readthedocs.io/en/latest) provides compression and memory mapping, and makes it possible to run all of the code on a fairly standard (16 GB RAM) machine.
Therefore some notebooks need to run before others. Particularly those which have 'to_asdf' in their file names...

Sooner or later the code will get old, and there will be dependency issues.
The [`environment_2022-01.yaml`](./environment_2022-01.yaml) file documents the library versions that I used at the time of this writing (January 2022).

### Quick Prepare/Install Notes

#### When Working on Anaconda

- Create a new virtual environment.
- Install all necessary packages that are available in anaconda repositories and as reasonbly recent versions. Typically one wants to use the `conda-forge` repo. (In my case these packages are `numpy`, `scipy`, `matplotlib`, `asdf`, `gdal`, `rasterio`, `obspy`, `shapely`, `fiona`.) Consider installing and using the `mamba` tool which does the same thing as `conda`, but is so much faster when resolving dependencies.
- Use `pip` to install the remaining necessary packages in more recent versions. (`ipython`, `jupyterlab`, `nbconvert`, `nbclassic`)
- Install the Python modules in the libs folder into the virtual environment using `pip`.

This likely looks something like:
```bash
# ... assumes a working anaconda setup and creates a virtual
# environment named '2018blasts. Also assumes that git is installed'
conda create -n 2018blasts
conda activate 2018blasts
conda install -c conda-forge mamba
mamba install -c conda-forge numpy scipy matplotlib asdf gdal rasterio obspy shapely fiona
pip install ipython jupyterlab nbconvert nbclassic
git clone https://gitlab.com/isonder/2018_blasts.git
cd 2018_blasts/libs
pip install --upgrade -e .
```

#### When Working on `pip` Only

This is typically a situation on Linux. Same steps as when working on anaconda, but more focus on `pip`.
- Install packages which need compilation using the package manager (typically this is `gdal`).
- Create a new virtual environment
- Install all remaining packages using `pip`.
- Install modules in the `libs` folder.


## License

[GPLv2](./LICENSE)